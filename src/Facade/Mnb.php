<?php

namespace Extradevs\Mnb\Laravel\Facade;

use Illuminate\Support\Facades\Facade;
use Extradevs\Mnb\Laravel\Client;

class Mnb extends Facade {

    protected static function getFacadeAccessor()
    {
        return Client::class;
    }


}