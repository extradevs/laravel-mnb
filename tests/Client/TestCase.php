<?php

namespace Extradevs\Mnb\Laravel\Tests\Client;

use Illuminate\Support\Facades\Cache;
use Mockery;
use Extradevs\Mnb\Laravel\Client;
use Extradevs\Mnb\Model\Currency;

class TestCase extends \Orchestra\Testbench\TestCase
{

    /**
     * @var Mockery\MockInterface
     */
    protected $mnb;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var int
     */
    protected $timeout;

    /**
     * @var array
     */
    protected $currencyList = ['EUR', 'BGP'];

    /**
     * @var Currency[]
     */
    protected $exchangeRateList;

    protected function setUp(): void
    {
        parent::setUp();

        $this->timeout = 10;
        $store         = Cache::store();
        $this->mnb     = Mockery::mock(\Extradevs\Mnb\Client::class);
        $this->client  = new Client($this->mnb, $store, $this->timeout);

        $this->exchangeRateList = [
            new Currency('EUR', 1, 300),
        ];
    }

}
